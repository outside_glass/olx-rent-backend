import { Module } from '@nestjs/common';
import { EventEmitter } from './event.emitter';

@Module({
  providers: [EventEmitter],
  exports: [EventEmitter],
})
export class CommonModule {}
