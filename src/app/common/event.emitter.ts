import { EventEmitter as BaseEventEmitter } from 'events';
import { Injectable } from '@nestjs/common';

@Injectable()
export class EventEmitter extends BaseEventEmitter {
}
