import { Module } from '@nestjs/common';
import { TelegramBotService } from './telegram-bot.service';
import { ConfigModule } from '@nestjs/config';
import { CommonModule } from '../common/common.module';
import { TelegramActionsModule } from '../telegram-actions/telegram-actions.module';

@Module({
  imports: [TelegramActionsModule, CommonModule, ConfigModule],
  providers: [TelegramBotService],
})
export class TelegramBotModule {
  constructor(botService: TelegramBotService) {
    botService.launch();
  }
}
