import { Injectable } from '@nestjs/common';
import Telegraf, { ContextMessageUpdate } from 'telegraf';
import { ConfigService } from '@nestjs/config';
import { EventEmitter } from 'events';
import { ActionType } from '../common/action.type';

@Injectable()
export class TelegramBotService {
  private readonly bot: Telegraf<any>;

  constructor(
    private configService: ConfigService,
    private readonly eventEmitter: EventEmitter,
  ) {

    this.bot = new Telegraf(process.env.BOT_TOKEN);

    Object.values(ActionType).forEach((type: string) => {
      this.bot.command(type, (ctx: ContextMessageUpdate) => {
        this.eventEmitter.emit(type, ctx);
      })
    })
  }

  async launch() {
    await this.bot.startPolling();
  }
}
