import { ConfigModule } from '@nestjs/config';
import { TelegramBotModule } from './telegram-bot/telegram-bot.module';
import { Module } from '@nestjs/common';
import { TelegramActionsModule } from './telegram-actions/telegram-actions.module';
import { CommonModule } from './common/common.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import ormconfig from '../ormconfig';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot(ormconfig),
    TelegramBotModule,
    TelegramActionsModule,
    CommonModule,
  ],
})
export class AppModule {}
