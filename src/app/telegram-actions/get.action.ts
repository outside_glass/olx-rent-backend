import { ContextMessageUpdate } from 'telegraf';
import { ActionType } from '../common/action.type';
import { Inject, Injectable } from '@nestjs/common';
import { ScraperService } from '../scraper/scraper.service';
import { BaseAction } from './base.action';
import getJokeScenario from '../scraper-scenarios/getJokeScenario';

@Injectable()
export class GetAction extends BaseAction {
  @Inject() private readonly scraperService: ScraperService;

  async do(ctx: ContextMessageUpdate): Promise<void> {
    await this.scraperService.run(getJokeScenario, { ctx });
  }

  getActionType(): ActionType {
    return ActionType.GET;
  }
}
