import { EventEmitter } from '../common/event.emitter';
import { ContextMessageUpdate } from 'telegraf';
import { ActionType } from '../common/action.type';
import { Injectable } from '@nestjs/common';

@Injectable()
export abstract class BaseAction {
  constructor(
    protected readonly eventEmitter: EventEmitter
  ) {
    this.eventEmitter.on(this.getActionType(), this.do.bind(this));
  }

  abstract getActionType(): ActionType;

  abstract do(ctx: ContextMessageUpdate): Promise<void>;
}
