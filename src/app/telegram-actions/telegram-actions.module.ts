import { Module } from '@nestjs/common';
import { CommonModule } from '../common/common.module';
import { GetAction } from './get.action';
import { ScraperModule } from '../scraper/scraper.module';

@Module({
  imports: [CommonModule, ScraperModule],
  providers: [GetAction],
})
export class TelegramActionsModule {}
