import { Browser } from 'puppeteer-core';

export default interface BaseScenario {
  (instance: Browser, params: object): Promise<void>;
}
