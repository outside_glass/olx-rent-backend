import { Browser } from 'puppeteer-core';
import { ContextMessageUpdate } from 'telegraf';

interface Params {
  ctx: ContextMessageUpdate;
}

export default async function getJokeScenario(instance: Browser, params: Params): Promise<void> {
  const { ctx } = params;

  const page = await instance.newPage();

  await page.goto(`https://www.anekdot.ru/random/anekdot/`, {
    waitUntil: 'domcontentloaded',
  });

  const joke = await page.$eval(
    'div.content div.topicbox[id] div.text',
    (element) => element.textContent);

  await ctx.reply(joke);
}
