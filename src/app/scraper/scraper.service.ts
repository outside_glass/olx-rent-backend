import { Injectable } from '@nestjs/common';
import * as puppeteer from 'puppeteer-core';
import { LaunchOptions, Browser } from 'puppeteer-core';
import { ConfigService } from '@nestjs/config';
import BaseScenario from '../scraper-scenarios/interfaces/base.scenario';

@Injectable()
export class ScraperService {
  private readonly options: LaunchOptions;

  constructor(
    private readonly configService: ConfigService,
  ) {
    this.options = {
      executablePath: this.configService.get<string>('PUPPETEER_EXECUTABLE_PATH'),
      headless: false,
      defaultViewport: {
        width: 1920,
        height: 1080,
      }
    };
  }

  async run(callback: BaseScenario, params: object): Promise<void> {
    let instance: Browser;

    try {
      instance = await puppeteer.launch(this.options);

      await callback(instance, params);
    } catch (err) {
      throw err;
    } finally {
      await instance.close();
    }
  }
}
